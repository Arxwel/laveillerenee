<?php
//Ce controleur récupère es nouvelles à afficher pour 'afficher_mes_nouvelles_img.view.php' ou rnevoie vers la vue d'erreur
include_once('../model/DAO.class.php');

if (!isset($_COOKIE['login'])) {
  include('../controler/non_connecte.ctrl.php');
} else {
  $flux = $dao->getUserFlux($_COOKIE['login']);

  if ($flux != NULL) {

    global $newsToDisplay;
    global $nbnouvelles;

    if (isset($_GET['form'])) {

      $args = array();
      $tri = NULL;

      if (isset($_GET['the_search'])) {
        $args[1] = $_GET['the_search'];
      } else {
        $args[1] = '';
      }

      if ($_GET['debut'] != '') {
        $args[2] = $_GET['debut'];
      } else {
        $args[2] = date('Y-m-d', 0);
      }

      if ($_GET['fin'] != '') {
        $args[3] = $_GET['fin'];
      } else {
        $args[3] = date('Y-m-d', time());
      }

      if (isset($_GET['triDate'])) {
        $tri = $_GET['triDate'];
      }

      foreach ($flux as $rss) {
        $dao->majNouvelles($rss->id);
        $args[0] = $rss->id;
        $newsToDisplay[] = $dao->getSpecificNouvellesFromRSS($args, $tri);
      }
    } else {
      foreach ($flux as $rss) {
        $dao->majNouvelles($rss->id);
        $newsToDisplay[] = $dao->getNouvellesFromRSS($rss->id);
      }
    }


    $nbnouvelles = count($newsToDisplay);
    include('../view/afficher_mes_nouvelles_img.view.php');
  } else {
    include("../view/erreur_pas_de_usernouvelles.view.php");
  }
}
 ?>
