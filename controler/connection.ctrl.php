<?php
//controleur de la connection d'un utilisateur
include('../model/DAO.class.php');
if (isset($_POST['login']) && isset($_POST['mdp'])) {
  include_once("../model/DAO.class.php");
  $logged = $dao->createSession($_POST['login'], $_POST['mdp']);
  if ($logged) {
    include("../controler/index.ctrl.php");
  } else {
    include("../view/connection_failure.view.php");
  }
} else {
  include("../view/formulaire_connection.view.php");
}
?>
