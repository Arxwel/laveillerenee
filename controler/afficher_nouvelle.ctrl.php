<?php
//controleur de l'affichage d'une nouvelle
 include_once("../model/DAO.class.php");
 if (!isset($_COOKIE['login'])) {
   include('../controler/non_connecte.ctrl.php');
 } else {
   if(!isset($_GET['id'])) {
     include("../view/erreur.view.php");
   }

   $id = $_GET['id'];

   global $new;
   $new = $dao->getNouvelle($id);

   include("../view/afficher_nouvelle.view.php");
 }
?>
