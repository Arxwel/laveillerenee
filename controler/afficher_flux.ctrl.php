<?php
//Ce controleur selection les flux à afficher par 'afficher_flux.view.php' et appelle la vue d'erreur si aucun flux n'est disponible
include_once("../model/DAO.class.php");
if (!isset($_COOKIE['login'])) {
  include('../controler/non_connecte.ctrl.php');
} else {

  global $fluxRSSToDisplay;
  $fluxFromDB = $dao->getFlux();

  if ($fluxFromDB == NULL) {
    include("../view/erreur_pas_de_flux.view.php");
  } else {

    foreach($fluxFromDB as $flux) {
      $flux->update();
      $dao->updateRSS($flux);
      $fluxRSSToDisplay[] = $flux;
    }
    include("../view/afficher_flux.view.php");
  }
}

?>
