<?php
//gère l'inscription d'un nouvel utilisateur
if (isset($_POST['login']) && isset($_POST['mdp'])) {
  include_once("../model/DAO.class.php");
  $check = $dao->ajouterUtilisateur($_POST['login'], $_POST['mdp']);
  if ($check) {
    include("../view/inscription_success.view.php");
  } else {
    include("../view/inscription_failure.view.php");
  }
} else {
  include("../view/formulaire_inscription.view.php");
}
?>
