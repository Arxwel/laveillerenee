<?php
//Ce controleur selection les flux à afficher par 'afficher_mes_flux.view.php' et appelle la vue d'erreur si aucun flux n'est disponible
include_once("../model/DAO.class.php");
if (!isset($_COOKIE['login'])) {
  include('../controler/non_connecte.ctrl.php');
} else {

  global $fluxRSSToDisplay;
  $fluxFromDB = $dao->getUserFlux($_COOKIE['login']);

  if ($fluxFromDB == NULL) {
    include("../view/erreur_pas_de_userflux.view.php");
  } else {
    foreach($fluxFromDB as $flux) {
      if ($flux != null) {
        $flux->update();
        $dao->updateRSS($flux);
        $fluxRSSToDisplay[] = $flux;
      }
    }
    include("../view/afficher_mes_flux.view.php");
  }
}

?>
