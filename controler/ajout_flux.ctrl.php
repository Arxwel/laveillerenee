<?php
//controle l'ajout d'un flux à la BD
if (!isset($_COOKIE['login'])) {
  include('../controler/non_connecte.ctrl.php');
} else {
  if (isset($_GET['urlRSS'])) {
    include_once("../model/DAO.class.php");
    $check = $dao->ajouterNouveauFlux($_GET['urlRSS']);
    if ($check) {
      include("../view/ajout_success.view.php");
    } else {
      include("../view/ajout_failure.view.php");
    }

  } else {
    include("../view/formulaire_ajout_flux.view.php");
  }
}

?>
