<?php
 include_once("../model/DAO.class.php");
 if (!isset($_COOKIE['login'])) {
   include('../controler/non_connecte.ctrl.php');
 } else {

   $rssId = $_GET['rssid'];

   if($rssId == NULL) {
     include("../view/erreur.view.php");
   }

   global $newsToDisplay;
   $dao->majNouvelles($rssId);

   if (isset($_GET['form'])) {
     $args = array();
     $args[0] = $rssId;
     if (isset($_GET['the_search'])) {
       $args[1] = $_GET['the_search'];
     } else {
       $args[1] = '';
     }

     if ($_GET['debut'] != '') {
       $args[2] = $_GET['debut'];
     } else {
       $args[2] = date('Y-m-d H:i:s', 0);
     }

     if ($_GET['fin'] != '') {
       $args[3] = $_GET['fin'];
     } else {
       $args[3] = date('Y-m-d H:i:s', time());
     }

     if (isset($_GET['triDate'])) {
       $tri = $_GET['triDate'];
     } else {
       $tri = NULL;
     }
     $newsToDisplay = $dao->getSpecificNouvellesFromRSS($args, $tri);
   } else {
     $newsToDisplay = $dao->getNouvellesFromRSS($rssId);
   }

   include("../view/afficher_nouvelles.view.php");
}

?>
