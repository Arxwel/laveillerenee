<?php
//Ce contrôleur gère l'abonnement d'un utilisateur à un flux
include_once('../model/DAO.class.php');
if (!isset($_GET['rssId'])) {
  include('../view/erreur.view.php');
} else {
  if (!isset($_COOKIE['login'])) {
    include('../controler/non_connecte.view.php');
  } else {
    $rssId = $_GET['rssId'];
    $login = $_COOKIE['login'];

    $dao->abonner($login, $rssId);
    include('../controler/afficher_flux.ctrl.php');
  }
}
?>
