<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Non connecté</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>

    <body>
    <nav>
        <?php include('../view/barre_menu.php'); ?>
    </nav>
    <div class="container">
        <div class="info">
            <h3>Vous devez être connecté pour accéder au contenu du site</h3>
            <br/>
                  <a href="../controler/connection.ctrl.php"><button class="ui positive button">Connection</button></a>
                  <a href="../controler/inscription.ctrl.php"><button class="ui button">Inscription</button></a>
        </div>
    </div>
    </body>
</html>
