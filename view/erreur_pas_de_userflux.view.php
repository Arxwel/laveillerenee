<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Erreur : Aucun flux à afficher</title>
        <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>

    <body>
    <nav>
        <?php include('../view/barre_menu.php'); ?>
    </nav>
    <div class="container">
        <div class="error">
            <h3>Vous n'êtes abonné à aucun flux</h3>
            <ul>
                <li>Vous devez vous <a href="../controler/afficher_flux.ctrl.php">abonner à un flux</a>. </li>
            </ul>
        </div>
    </div>
    </body>
</html>
