<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Nouvelles</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>

    <body>
    <nav>
        <?php include('../view/barre_menu.php'); ?>
    </nav>
    <div class="container2">
      <h1>Toutes les Nouvelles</h1>
      <form action="../controler/afficher_nouvelles_img.ctrl.php" method="get">
        <h3>Rechercher une nouvelle</h3>
        <input type="hidden" name="form" value="true">
          <fieldset>
            <legend>Par mot-clef</legend>
              <label for="the_search">Mot-clef : </label>
              <input type="search" placeholder="Entrez un mot-clef" name="the_search" id="the_search">
          </fieldset>
          <fieldset>
            <legend>Par date</legend>
              <label for="debut">Entre </label>
              <input type="date" placeholder="aaaa-mm-dd" name="debut" id="debut">
              <label for="fin">et</label>
              <input type="date" placeholder="aaaa-mm-dd" name="fin" id="fin">
          </fieldset>
          <fieldset>
            <legend>Trier par date</legend>
              <input type="radio" name="triDate" id="croissant">
              <label for="croissant">Croissant</label>
              <input type="radio" name="triDate" id="decroissant">
              <label for="decroissant">Décroissant</label>
          </fieldset>
          <button type="submit"> Envoyer</button>
      </form>
      <?php
    foreach($newsToDisplay as $news) {
              foreach($news as $nouvelles) {
                  printf("<article>\n");
                  printf("<h2>%s</h2>\n",$nouvelles->titre);
                  printf("<a href=\"../controler/afficher_nouvelle.ctrl.php?id=%s\">\n",$nouvelles->id);
                  if($nouvelles->image != NULL) {
                      printf("<img class=\"mosaique\" src=\"%s\">\n", $nouvelles->image);
                  } else {
                      printf("<img class=\"mosaique\" src=\"%s\">\n", "http://d3cc34wxvev8pg.cloudfront.net/static/curtains/img/image-not-available.png");
                  }
                  printf("</a>\n</article>\n");
              }
    }
    ?>
      </div>
    </body>
</html>
