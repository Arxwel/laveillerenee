<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Ajouter un Flux RSS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>
    <body>
    <nav>
        <?php include('../view/barre_menu.php'); ?>
    </nav>
    <div class="container">
      <form action="../controler/ajout_flux.ctrl.php" method="GET">
          <h1>Ajouter un Flux RSS</h1>
          <div class="field">
              <label for="urlRSS">Entrez un Flux RSS à Suivre:</label>
              <input type="text" name="urlRSS" id="urlRSS" placeholder="URL vers le flux RSS" required/>
          </div>
        <button class="ui button" type="submit">Envoyer</button>
      </form>
    </div>
    </body>
</html>
