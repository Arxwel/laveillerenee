<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Nouvelles</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>

    <body>
      <nav>
          <?php include('../view/barre_menu.php'); ?>
      </nav>
      <div class="container">
        <h2>Nouvelles</h2>
        <hr />
          <form action="../controler/afficher_nouvelles.ctrl.php" method="get">
            <h3>Rechercher une nouvelle</h3>
            <input type="hidden" name="form" value="true">
            <input type="hidden" name="rssid" value ="<?= $_GET['rssid'] ?>">
              <fieldset>
                <legend>Par mot-clef</legend>
                  <label for="the_search">Mot-clef : </label>
                  <input type="search" placeholder="Entrez un mot-clef" name="the_search" id="the_search">
              </fieldset>
              <fieldset>
                <legend>Par date</legend>
                  <label for="debut">Entre </label>
                  <input type="date" placeholder="aaaa-mm-dd" name="debut" id="debut">
                  <label for="fin">et</label>
                  <input type="date" placeholder="aaaa-mm-dd" name="fin" id="fin">
              </fieldset>
              <fieldset>
                <legend>Trier par date</legend>
                  <input type="radio" name="triDate" id="croissant">
                  <label for="croissant">Croissant</label>
                  <input type="radio" name="triDate" id="decroissant">
                  <label for="decroissant">Décroissant</label>
              </fieldset>
              <button type="submit"> Envoyer</button>
          </form>
          <hr />
      <?php foreach($newsToDisplay as $news) {
        printf("<article>\n");
        printf("<h2>%s</h2>\n", $news->titre);
        printf("<p class=\"datemeta\">%s</p>\n", $news->date);
        printf("<img clas)\"mosaique\" src=\"%s\" alt=\"s%\" />\n", $news->image, $news->titre);
        printf("<p class=\"bottom\"><a href=\"../controler/afficher_nouvelle.ctrl.php?id=%s\"> Voir la nouvelle </a></p>\n", $news->id);
        printf("</article>\n");
      } ?>
    </div>
    </div>
    </body>
</html>
