<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Erreur</title>
    <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>
    <body>
    <nav>
        <?php include('../view/barre_menu.php'); ?>
    </nav>
    <div class="container">
        <div class="error">
            <h3>
                Une erreur est survenue lors de l'utilisation du service.
            </h3>
        </div>
    </div>
    </body>
</html>
