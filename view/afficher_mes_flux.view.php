<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>FLUX</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>

    <body>
      <nav>
          <?php include('../view/barre_menu.php'); ?>
      </nav>
      <div class="ui raised container segment">
          <h1 class="ui header">Liste des Flux RSS suivis</h1>
          <div class="ui cards">
      <?php
      foreach($fluxRSSToDisplay as $flux) {
        printf("<article>\n");
        printf("<h2>%s</h2>\n", $flux->titre);
        printf("<p class=\"datemeta\">%s</p>\n", $flux->date);
        printf("<div class=\"bottom\">\n");
        printf("<a href=\"../controler/afficher_nouvelles.ctrl.php?rssid=%s\"> Voir le contenu du Flux </a>\n",$flux->id);
        printf("<a href=\"../controler/desabonner.ctrl.php?rssId=%s\"><button>Se désabonner</button></a><br/>\n", $flux->id);
        printf("</div>\n</article>\n");
      } ?>
      </div>
      </div>
    </body>
</html>
