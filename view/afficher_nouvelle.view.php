<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?= $new->titre ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>

    <body>
        <nav>
            <?php include('../view/barre_menu.php'); ?>
        </nav>
        <div class="container">
            <h2><?= $new->titre ?></h2>
            <p>
                <p class="datemeta"><?= $new->date ?></p>
            <?php if($new->image != NULL) {
                    printf("<img class=\"centered large ui image\" src=\"%s\" alt=\"%s\" />\n", $new->image, $new->titre);
                  }
            ?>
            </p>
            <br />
            <a href="<?= $new->url ?>"> Voir la News </a>
        </div>
    </body>
</html>
