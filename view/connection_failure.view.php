<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Echec Connection</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>
    <body>
    <nav>
        <?php include('../view/barre_menu.php'); ?>
    </nav>
    <div class="container">
        <div class="error">
            <h3>La Connection a échoué</h3>
            <ul>
                <li>Votre Login ou Mot de Passe est incorrect</li>
            </ul>
        </div>
    </div>
    </body>
</html>
