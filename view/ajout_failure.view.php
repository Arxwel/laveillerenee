<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Erreur Saisie de Flux</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>

    <body>
    <nav>
        <?php include('../view/barre_menu.php'); ?>
    </nav>
    <div class="container">
    <div class="error">
        <h3>Le Flux saisi est invalide</h3>
        <ul>
            <li>L'URL entrée ne pointe pas vers un Flux RSS valide</li>
            <li>Vous n'avez pas saisi une URL</li>
        </ul>
    </div>
    </div>
    </body>
</html>
