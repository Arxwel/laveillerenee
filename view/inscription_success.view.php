<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Inscription Réussie</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>

    <body>
    <nav>
        <?php include('../view/barre_menu.php'); ?>
    </nav>
    <div class="container">
        <div class="success">
            <h3>L'Inscription est un succès</h3>
            <ul>
                <li><a href="../controler/connection.ctrl.php">Connectez-vous</a>
            </ul>
        </div>
    </div>
    </body>
</html>
