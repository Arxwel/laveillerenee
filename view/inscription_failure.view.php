<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Echec Inscription</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>

    <body>
    <div class="container">
        <div class="error">
            <h3>L'Inscription a échoué</h3>
            <ul>
                <li>Réessayez avec un Login différent</li>
            </ul>
        </div>
    </div>
    </body>
</html>
