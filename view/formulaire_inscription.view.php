<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Inscription</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>
    <body>
    <nav>
        <?php include('../view/barre_menu.php'); ?>
    </nav>
    <div class="container">
        <a href="../controler/connection.ctrl.php"><button class="buttonRightBlue">Connexion</button></a>
      <h1>Inscription</h1>
      <form class="ui form" action="../controler/inscription.ctrl.php" method="POST">
          <h4>Saisissez vos informations pour vous inscrire</h4>
              <div class="field">
                  <label>Login:</label>
                  <input type="text" name="login" id="login" maxlength = 80 required/>

              </div>
              <div class="field">
                  <label>Mot de Passe:</label>
                  <input type="password" name="mdp" id="mdp" maxlength = 8 required/>
              </div>
          <button class="ui button" type="submit">Envoyer</button>
      </form>
    </div>
    </body>
</html>
