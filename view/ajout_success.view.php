<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Flux ajouté avec succès</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../view/css/Style.css">
    </head>

    <body>
    <nav>
        <?php include('../view/barre_menu.php'); ?>
    </nav>
    <div class="container">
        <div class="success">
            <h3>Le Flux a été ajouté à la base de données !</h3>
            <ul>
                <li>Vous pouvez dès à présent revenir au Menu principal.</li>
            </ul>
        </div>
    </div>
    </body>
</html>
