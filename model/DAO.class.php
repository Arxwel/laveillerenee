<?php
include_once("../model/RSS.class.php");
include_once("../model/Nouvelle.class.php");

$dao = new DAO();

class DAO {
  private $db;

  function __construct() {
    $dataSource = 'sqlite:../data/rss.db';
    try {
      $this->db = new PDO($dataSource);
    } catch (PDOException $e) {
      exit ('[ERREUR OUVERTURE PDO]'.$e->getMessage());
    }
  }

  ////Fonctions relative au Flux RSS
  function ajouterNouveauFlux($urlRSS) { //retourne true en cas de réussite et false sinon
    $rssAjout = new RSS();
    $rssAjout->url = $urlRSS;
    $isRss = $rssAjout->update(); //remplit les champs titre, date
    if ($isRss) {
      $this->majRSS($rssAjout);
      $id = $this->getRSSID($rssAjout->url);
      $rssAjout->id = $id;
      return TRUE;
    } else {
      return FALSE;
    }
  }


  function majRSS($rss) { //Ajoute le RSS donné à la BD si celui ci n'y est pas déjà, sinon met à jour ses valeurs dans la BD
    $id = $this->getRSSID($rss->url);
    if ($id == -1) {
      $this->insertRSS($rss);
    } else {
      $this->updateRSS($rss);
    }
  }

  function insertRSS($rss) {//retourne true en cas de réussite et false sinon

    $titre = $rss->titre;
    $url = $rss->url;
    $date = $rss->date;
    try {
    $insert = $this->db->prepare('INSERT INTO RSS(titre, url, date) VALUES (?, ?, ?);');
    /*
    $a = $insert->bindParam(':titre', $titre);
    $b = $insert->bindParam(':url', $url);
    $c = $insert->bindParam(':date', $date);

    var_dump($a);
    var_dump($b);
    var_dump($c);
*/

    $check = $insert->execute(array($titre,$url,$date));
    $insert->closeCursor();
    return $check;
    } catch (PDOException $e) {
        die("[Erreur PDO]" . $e->getMessage());
    }
  }

  function updateRSS($rss) {//retourne true en cas de réussite et false sinon
    try {
    $update = $this->db->prepare('UPDATE RSS SET titre=:titre, date=:date WHERE url=:url;');

    $url = $rss->url;
    $date = $rss->date;
    $titre = $rss->titre;

    $update->bindParam(':url', $url);
    $update->bindParam(':date', $date);
    $update->bindParam(':titre', $titre);

    $check = $update->execute();
    $update->closeCursor();
    return $check;
    } catch (PDOException $e) {
        die("[Erreur PDO]" . $e->getMessage());
    }
  }

  function getRSSID($url) {//renvoi l'Id d'un RSS ou -1 si celui ci est introuvable
    $urlien = $url;

    $select = $this->db->prepare('SELECT * FROM RSS WHERE url = ?;');

    $select->execute(array($urlien));
    $flux = $select->fetchAll(PDO::FETCH_CLASS, 'RSS');
    if (!empty($flux)) {
      $id = $flux[0]->id;
    } else {
      $id = -1;
    }

    $select->closeCursor();
    return $id;
  }

  function getFlux() {//retourne tout les flux de la BD
    $select = $this->db->prepare('SELECT * FROM RSS;');

    $select->execute();

    $flux = $select->fetchAll(PDO::FETCH_CLASS, 'RSS');

    $select->closeCursor();
    return $flux;
  }

  function getRSS($id) {//retourne le flux ayant l'id $id sinon retourne null
    $select = $this->db->prepare("SELECT * FROM RSS WHERE id = :id;");

    $select->bindParam(':id', $id);

    $select->execute();
    $flux = $select->fetchAll(PDO::FETCH_CLASS, 'RSS');

    if ($flux == NULL) {
      return NULL;
    }

    $select->closeCursor();
    return $flux[0];
  }



//// Fonctions relatives aux Nouvelles



  function majNouvelles($rssId) { //met à jour toute les nouvelles (cf majNouvelle($nouvelle, $rssId))
    $rss = $this->getRSS($rssId);
    $nouvellesFromFlux = $rss->getNouvelles();
    foreach($nouvellesFromFlux as $nouvelle) {
      $this->majNouvelle($nouvelle, $rssId);
    }
  }

  function getNouvellesFromRSS($rssId) { //retourne toutes les nouvelles d'un flux
    $select = $this->db->prepare('SELECT * FROM nouvelle WHERE RSS_id = :rssId');

    $select->bindParam(':rssId', $rssId);

    $select->execute();

    $nouvelles = $select->fetchAll(PDO::FETCH_CLASS, 'Nouvelle');

    $select->closeCursor();
    return $nouvelles;
  }

  function getSpecificNouvellesFromRSS($args, $tri) {
    //renvoie les nouvelles en fonction des spécification contenues dans $args
    //mot clé, date, tri
    $query = 'SELECT * FROM nouvelle WHERE RSS_id = :rssId AND (titre LIKE :search OR description LIKE :search) AND date > :debut AND date < :fin';
    if ($tri != NULL) {
      if ($tri == 'croissant') {
        $query .= ' ORDER BY date';
      } else {
        $query .= ' ORDER BY date DESC';
      }
    }
    $query .= ';';
    $select = $this->db->prepare($query);

    $select->bindParam(':rssId', $args[0]);
    $search = '%'.$args[1].'%';
    $select->bindParam(':search', $search);
    $select->bindParam(':debut', $args[2]);
    $select->bindParam(':fin', $args[3]);

    $select->execute();

    $nouvelles = $select->fetchAll(PDO::FETCH_CLASS, 'Nouvelle');

    $select->closeCursor();
    return $nouvelles;
  }

  function majNouvelle($nouvelle, $rssId) { //ajoute la Nouvelle à la BD si elle n'y est pas déjà, sinon met à jour la BD
    $id = $this->getNouvelleId($nouvelle->url);
    if ($id == -1) {
      $this->insertNouvelle($nouvelle, $rssId);
    } else {
      $this->updateNouvelle($nouvelle);
    }
  }

  function insertNouvelle($nouvelle, $rssId) {//retourne true en cas de réussite et false sinon
    $insert = $this->db->prepare('INSERT INTO nouvelle(titre, date, description, url, image, RSS_id) VALUES (:titre, :date, :description, :url, :image, :rssId);');

    $titre = $nouvelle->titre;
    $date = $nouvelle->date;
    $description = $nouvelle->description;
    $url = $nouvelle->url;
    $urlImage = $nouvelle->image;

    $insert->bindParam(':titre', $titre);
    $insert->bindParam(':date', $date);
    $insert->bindParam(':description', $description);
    $insert->bindParam(':url', $url);
    $insert->bindParam(':image', $urlImage);
    $insert->bindParam(':rssId', $rssId);

    $check = $insert->execute();
    $insert->closeCursor();
    return $check;
  }

  function updateNouvelle($nouvelle) {//retourne true en cas de réussite et false sinon
    $update = $this->db->prepare('UPDATE nouvelle SET titre = :titre,  date = :date, description = :description, url = :url, image = image;');

    $titre = $nouvelle->titre;
    $date = $nouvelle->date;
    $description = $nouvelle->description;
    $url = $nouvelle->url;
    $urlImage = $nouvelle->image;

    $update->bindParam(':titre', $titre);
    $update->bindParam(':date',$date);
    $update->bindParam(':description',$description);
    $update->bindParam(':url',$url);
    $update->bindParam(':image',$urlImage);

    $check = $update->execute();
    $update->closeCursor();
    return $check;
  }

  function getNouvelleId($url) {//return -1 si url introuvable
    $select = $this->db->prepare('SELECT id FROM nouvelle WHERE url = :url;');

    $select->bindParam(':url', $url);

    $select->execute();
    $result = $select->fetchAll();

    if ($result != null) {
      $id = $result[0]['id'];
    } else {
      $id = -1;
    }

    $select->closeCursor();
    return $id;
  }

    function getNouvelle($id) {// renvoie la nouvelle d'id $id
        $select = $this->db->prepare("SELECT * FROM Nouvelle WHERE id = :id;");

        $select->bindParam(':id', $id);

        $select->execute();
        $nouv = $select->fetchAll(PDO::FETCH_CLASS, 'Nouvelle');

        if ($nouv == NULL) {
            return NULL;
        }

        $select->closeCursor();
        return $nouv[0];
    }



//// Fonctions relatives aux Utilisateurs




    function ajouterUtilisateur($login, $mdp) { //retourne TRUE en cas de succes,  FALSE sinon
      if (!$this->userExists($login)) {
        $insert = $this->db->prepare('INSERT INTO utilisateur(login, mp) VALUES (:login, :mdp);');

        $insert->bindParam(':login', $login);
        $insert->bindParam(':mdp', $mdp);

        $check = $insert->execute();
        if ($check) {
          return TRUE;
          $insert->closeCursor();
        }
      }
      return FALSE;
    }

    function userExists($login) { //retourne TRUE si l'utilisateur existe, sinon FALSE
      $select = $this->db->prepare('SELECT * FROM utilisateur WHERE login = :login;');

      $select->bindParam(':login', $login);

      $select->execute();
      $user = $select->fetchAll();

      $select->closeCursor();
      return ($user != NULL);
    }



    function logValid($login, $mdp) {//renvoie TRUE si le couple login/mdp(mot de passe) est valide sinon FALSE
      $select = $this->db->prepare('SELECT * FROM utilisateur WHERE login = :login AND mp = :mdp;');
      $select->bindParam(':login', $login);
      $select->bindParam(':mdp', $mdp);

      $select->execute();

      $result = $select->fetchAll();
      $select->closeCursor();

      return ($result != NULL);
    }



//// Fonction Relatives aux "Sessions"




    function createSession($login, $mdp) { //renvoie l'Id de la session si login et MDP valide, sinon renvoie -1
      if ($this->logValid($login, $mdp)) {
        setCookie('login', $login, time() + 3600);
        return TRUE;
      }
      return FALSE;
    }

    function annihilateSession() { //met fin à la connextion de l'utilisateur
      setCookie('login', '', time() - 1);
    }



//// Fonctions relatives aux abonnements




    function getUserFlux($login) {//renoie les flux auquel l'utilisateur est abonné
      $flux = array();
      $select = $this->db->prepare('SELECT RSS_id FROM abonnement WHERE login = :login;');

      $select->bindParam(':login', $login);

      $select->execute();

      $rssId = $select->fetchAll();

      $select->closeCursor();

      $rss = array();
      foreach ($rssId as $id) {
        $rss[] = $this->getRSS($id[0]);
      }
      return $rss;
    }

    function abonner($login, $rssId) {//abonne l'utilisateur à un flux
      $insert = $this->db->prepare("INSERT INTO abonnement VALUES (:login, :rssId, '', '')");

      $insert->bindParam(':login', $login);
      $insert->bindParam(':rssId', $rssId);

      $insert->execute();

      $insert->closeCursor();
    }

    function desabonner($login, $rssId) {//désabonne l'utilisateur d'un flux
      $insert = $this->db->prepare('DELETE FROM abonnement WHERE login = :login AND RSS_id = :rssId;');

      $insert->bindParam(':login', $login);
      $insert->bindParam(':rssId', $rssId);

      $insert->execute();

      $insert->closeCursor();
    }

    function isAbonne($login, $rssId) {//renvoie true si l'utilisateur est abonné à un flux
      $select = $this->db->prepare('SELECT * FROM abonnement WHERE login = :login AND RSS_id = :rssId;');

      $select->bindParam(':login', $login);
      $select->bindParam(':rssId', $rssId);

      $select->execute();
      $result = $select->fetchAll();

      $select->closeCursor();

      return ($result != NULL);
    }

}
 ?>
