<?php
include_once("../model/DAO.class.php");

class RSS {
  private $id;
  private $titre;
  private $url;
  private $date;

  function __get($name) {
    return $this->$name;
  }

  function __set($name, $value) {
    $this->$name = $value;
  }

  function update() { //retourne TRUE en cas de Succes, FALSE sinon
      $doc = new DOMDocument;
      libxml_use_internal_errors(true);
      if ($doc->load($this->url)) {
        $nodeList = $doc->getElementsByTagName('title');
        $this->titre = $nodeList->item(0)->textContent;
        $this->date = $doc->getElementsByTagName('pubDate')->item(0)->textContent;
        return TRUE;
      } else {
          foreach (libxml_get_errors() as $error) {
              // handle errors here
          }
          libxml_clear_errors();
          libxml_use_internal_errors(false);
        return FALSE;
      }
  }

  function getNouvelles() {
    $doc = new DOMDocument;
    $doc->load($this->url);
    $nodeNouvelles =  $doc->getElementsByTagName('item');
    $nouvelles = array();
    foreach ($nodeNouvelles as $node) {
      $nouvelle = new Nouvelle();
      $nouvelle->update($node);
      $nouvelles[] = $nouvelle;
    }
    return $nouvelles;
  }
}


 ?>
