<?php
include_once("../model/DAO.class.php");

class Nouvelle {
  private $id;
  private $titre;
  private $date;
  private $description;
  private $url;
  private $image;


  function __get($name) {
    return $this->$name;
  }

  function __set($name, $value) {
    $this->$name = $value;
  }

  function update(DOMElement $node) {
    $this->titre = $node->getElementsByTagName('title')->item(0)->textContent;
    $this->date = date('Y-m-d H:i:s', strtotime($node->getElementsByTagName('pubDate')->item(0)->textContent));
    $this->description = $node->getElementsByTagName('description')->item(0)->textContent;
    $this->url = $node->getElementsByTagName('link')->item(0)->textContent;
    $nodeList = $node->getElementsByTagName('enclosure');
    if($nodeList->length != 0) {
      $this->image = $nodeList->item(0)->getAttribute('url');
    } else {
      $this->image = "";
    }
  }
}


 ?>
